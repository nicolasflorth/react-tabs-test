# README #

# Author: Nicolae Florescu #
# Portfolio URI: www.nicolaeflorescu.net #

# Set up steps: #
Run npm install
Run gulp

# About the test #
The test is done in React.
I got stucked when I had to save the new value from the input, after clicking on the tab name.
This is my first thing done in React, and I spend all my weekend on it to learn and understand React (I specifically wanted to do it in React).
I still have problems with understanding props and I couldn't pass a function from one component to another with props. 
Probably Redux is the way, or maybe it can work in the way I done it.
I need to do some more research.

The project is responsive and the layout is changed on width less than 780px.