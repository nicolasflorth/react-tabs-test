/*
*	Tab info with edit capability
*	Author: Nicolae Florescu
*	===========================================================================
*/

import React from 'react';
import PropTypes from 'prop-types';

const tabs = [{ 
				name: 'C-3PO',
				points: '2,983',
				image: 'images/C3P-0.svg'
			}, {
				name: 'Dart Vader', 
				points: '2,676',
				image: 'images/vader.svg'
			}, {
				name: 'BB-8',
				points: '1,292',
				image: 'images/bb-8.svg'
			}, {
				name: 'Boba Fett',
				points: '872',
				image: 'images/fett.svg'
			}];

export default class Tabs extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			tabs,
			activeTabIndex: 0,
			windowWidth: window.innerWidth,
			width: 780
		};
		this.handleTabClick = this.handleTabClick.bind(this);
	}

	handleTabClick(tabIndex) {
		this.setState({
			activeTabIndex: tabIndex === this.state.activeTabIndex ? this.props.defaultActiveTabIndex : tabIndex
		});
	}
	
	handleResize(e) {
        this.setState({
			windowWidth: window.innerWidth
        });
    }
	
	componentDidMount() {
        window.addEventListener('resize', this.handleResize.bind(this))
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize.bind(this))
    }

	// Encapsulate <Tabs/> component API as props for <Tab/> children
	renderChildrenWithTabsApiAsProps() {
		return React.Children.map(this.props.children, (child, index) => {
			return React.cloneElement(child, {
				onClick : this.handleTabClick,
				tabIndex: index,
				isActive: index === this.state.activeTabIndex
			});
		});
	} 
		
	// Render current active tab content
	renderActiveTabContent() {
		const {children} = this.props;
		const {activeTabIndex} = this.state;
		if(children[activeTabIndex]) {
			return children[activeTabIndex].props.children;
		}
	}

	// Render the content tabs only in a list
	renderAllTabs() {
		const {children} = this.props;
		const {activeTabIndex} = this.state;
		if(children) {
			var items = [];
			for (var i = 0; i < children.length; i++) {
				items.push(children[i].props.children);
			}
			return (
			   <div className="content-only">
					{items}
			   </div>
			);			
		}
		
	}
	
	render() {
		if(window.innerWidth > this.state.width){
			return (			
				<div className="tabs">
					<ul className="tabs-nav">
						{this.renderChildrenWithTabsApiAsProps()}
					</ul>
					<div className="tabs-active-content">
						{this.renderActiveTabContent()}
					</div>
				</div>
			)
		}else{
			return(	
				<div className="tabs-active-content">			
					{this.renderAllTabs()}
				</div>
			)
		}
	}
};

Tabs.PropTypes = {
  defaultActiveTabIndex: PropTypes.number
};

Tabs.defaultProps = {
  defaultActiveTabIndex: null
};

export class Tab extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.handleTabClick = this.handleTabClick.bind(this);
	}

	handleTabClick(event) {
		event.preventDefault();
		this.props.onClick(this.props.tabIndex);
	}

	render() {
		return (
			<li className="tab">
				<a className={`tab-link ${this.props.linkClassName} ${this.props.isActive ? 'active' : ''}`} onClick={this.handleTabClick}>
				{this.props.tabName}
				</a>
			</li>
		);
	}
}

Tab.PropTypes = {
  onClick      	: PropTypes.func,
  tabIndex     	: PropTypes.number,
  isActive     	: PropTypes.bool,
  tabName		: PropTypes.string.isRequired,
  linkClassName	: PropTypes.string.isRequired
};

export class Content extends React.Component {
	constructor(props) {		
		super(props);
		this.state = {
			editing: false
		};
		this.edit = this.edit.bind(this);
		this.save = this.save.bind(this);
		this.handler = this.handler.bind(this);
	}
	
	handler(e){
		this.props.updateName(e.target.value);
	}
  
	edit() {
		this.setState({
			editing: true
		});
	}
	
	save() {
		this.setState({
			editing: false
		});
		console.log( this.props );
		this.props.updateName(this.refs.newName.value, this.props.index)
	}
	
	renderNormal() {
		return(
			<div>
				<img src={this.props.imgSrc} alt={this.props.tabName} />
				<h3 onClick={this.edit}>{this.props.tabName}</h3>
				<div className="points">{this.props.points} points</div>
			</div>
		);
	}
	
	renderForm() {
		return(	
			<div>
				<img src={this.props.imgSrc} alt={this.props.tabName} />
				<input ref="newName" type="text" value={this.props.tabName} onChange={this.save}/>
				<button onClick={this.save}>Finish editing</button>
				<div className="points">{this.props.points} points</div>
			</div>
		);
	}
	
	render() {
		if(this.state.editing){
			return (
				this.renderForm()
			);
		}else{
			return (
				this.renderNormal()
			);
		}
	}
}

export class App extends React.Component {
	constructor(props) {
		super(props);
		this.updateName = this.updateName.bind(this);
		this.state = {
			tabs: tabs
		};
	}
	
	updateName(newName, i) {
		var arr = this.state.tabs;
		arr[i] = newName;
		this.setState({tabs: arr})
	}
	
	render() {
		return (
			<Tabs>
				{this.state.tabs.map((tab, index) =>
					<Tab key={index} index={index} iconClassName={'fa fa-headphones'} linkClassName={'custom-link'} tabName={tab.name} updateName={this.updateName}>
						<Content tabName={tab.name} imgSrc={tab.image} points={tab.points} />
					</Tab>					
				)}
			</Tabs>
		)
	}
}
