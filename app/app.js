/*
*	Author: Nicolae Florescu
*	===========================================================================
*/

import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';


import Tabs, {Tab,App} from './Tabs';

ReactDOM.render(<App/> , document.getElementById('container'));
